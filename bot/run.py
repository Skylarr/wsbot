#!/usr/bin/env python3
import logging

from workshopbot.bot import WorkshopBot
from workshopbot import databasemagic


def main():
    logging.basicConfig(level=logging.INFO)
    logging.info("Initializing...")

    databasemagic.init()

    # This is used by PyCharm, importing the file registers commands
    # noinspection PyUnresolvedReferences
    import workshopbot.commandmodules

    bot = WorkshopBot()
    bot.run()


if __name__ == "__main__":
    main()
