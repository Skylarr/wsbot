import discord

from workshopbot.bot import bot_command, WorkshopBot
from workshopbot.commands import ICommand
from workshopbot.parsing import UserCommand


@bot_command
class PingCommand(ICommand):
    async def execute(self, bot: WorkshopBot, message: discord.Message, user_cmd: UserCommand):
        await message.channel.send("Pong!")

    @classmethod
    def get_command_name(cls):
        return "ping"

    @classmethod
    def get_summary_text(cls):
        return "Pong!"

    @classmethod
    def get_help_text(cls):
        return "Makes the bot reply with \"Pong!\"\n" \
               "**Usage**: %s" % cls.get_command_name()
