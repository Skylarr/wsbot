from .help_command import *
from .ping_command import *
from .server_info_command import *
from .bot_info_command import *
from .discord_silver_command import *
from .purge_command import *

command_modules = [
    help_command,
    ping_command,
    server_info_command,
    bot_info_command,
    discord_silver_command,
    purge_command
]
