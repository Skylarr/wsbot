import sqlite3
from typing import List

import discord

from workshopbot import user_permissions
from workshopbot.bot import bot_command, WorkshopBot
from workshopbot.commands import ICommand
from workshopbot.parsing import UserCommand


@bot_command
class AddServerGroupUsersCommand(ICommand):
    async def execute(self, bot: WorkshopBot, message: discord.Message, user_cmd: UserCommand):
        # Check if we're in a DM or something
        if message.guild is None:
            em = discord.Embed(
                title="⚠️ Cannot manage server permissions outside of a server",
                color=0xF44336
            )
            await message.channel.send(embed=em)
            return

        if user_cmd.argc < 3:
            em = discord.Embed(
                title="⚠️ Invalid number of arguments",
                description="Usage: %s permission-group-name users..." % self.get_command_name(),
                color=0xF44336
            )
            await message.channel.send(embed=em)
            return

        # Check if the group exists
        uuid = user_permissions.get_guild_group_uuid_from_name(message.guild.id, user_cmd.argv[1])

        if uuid is None:
            em = discord.Embed(
                title="⚠️ Server permission group %s doesn't exist" % user_cmd.argv[1],
                color=0xF44336
            )
            await message.channel.send(embed=em)
            return

        # Add users to the group
        added_users = 0
        already_in_users = 0
        for user in message.mentions:
            try:
                user_permissions.add_user_to_guild_group(int(user.id), uuid)
                added_users += 1
            except sqlite3.IntegrityError:
                # The user was already in the group
                already_in_users += 1

        if already_in_users > 0:
            already_in_str = "%d user(s) were already in the group\n" % already_in_users
        else:
            already_in_str = ""

        em = discord.Embed(
            title="🔐 Added %d user(s) to %s" % (added_users, user_cmd.argv[1]),
            description="%s**Group UUID**: `%s`" % (already_in_str, uuid),
            color=0x2196F3
        )
        await message.channel.send(embed=em)

    @classmethod
    def get_command_name(cls):
        return "add-server-group-users"

    @classmethod
    def get_summary_text(cls):
        return "Assigns users to a server permission group"

    @classmethod
    def get_help_text(cls):
        return "Assigns users to a server permission group\n" \
               "**Usage**: %s permission-group-name users..." % cls.get_command_name()

    # Temporary
    @classmethod
    def get_global_permission_group_whitelist(cls) -> List[str]:
        return ["developer"]

    @classmethod
    def get_global_permission_group_blacklist(cls) -> List[str]:
        return ["@everyone"]
