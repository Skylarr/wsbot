from typing import List

import discord

from workshopbot import user_permissions
from workshopbot.bot import bot_command, WorkshopBot
from workshopbot.commands import ICommand
from workshopbot.parsing import UserCommand


@bot_command
class AddServerGroupCommand(ICommand):
    async def execute(self, bot: WorkshopBot, message: discord.Message, user_cmd: UserCommand):
        # Check if we're in a DM or something
        if message.guild is None:
            em = discord.Embed(
                title="⚠️ Cannot manage server permissions outside of a server",
                color=0xF44336
            )
            await message.channel.send(embed=em)
            return

        if user_cmd.argc != 2:
            em = discord.Embed(
                title="⚠️ Invalid number of arguments",
                description="Usage: %s name" % self.get_command_name(),
                color=0xF44336
            )
            await message.channel.send(embed=em)
            return

        uuid = user_permissions.add_permission_group(message.guild.id, user_cmd.argv[1])

        em = discord.Embed(
            title="🔐 Added server permission group %s" % user_cmd.argv[1],
            description="**UUID**: `%s`\n" % uuid,
            color=0x2196F3
        )
        await message.channel.send(embed=em)

    @classmethod
    def get_command_name(cls):
        return "add-server-group"

    @classmethod
    def get_summary_text(cls):
        return "Adds a new permission group to the server"

    @classmethod
    def get_help_text(cls):
        return "Adds a new permission group to the server\n" \
               "**Usage**: %s name" % cls.get_command_name()

    # Temporary
    @classmethod
    def get_global_permission_group_whitelist(cls) -> List[str]:
        return ["developer"]

    @classmethod
    def get_global_permission_group_blacklist(cls) -> List[str]:
        return ["@everyone"]
