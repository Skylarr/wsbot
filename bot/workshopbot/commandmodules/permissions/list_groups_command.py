import discord

from workshopbot import user_permissions
from workshopbot.bot import bot_command, WorkshopBot
from workshopbot.commands import ICommand
from workshopbot.parsing import UserCommand


@bot_command
class ListGroupsCommand(ICommand):
    async def execute(self, bot: WorkshopBot, message: discord.Message, user_cmd: UserCommand):
        em = discord.Embed(
            title="🔐 Permission groups",
            color=0x2196F3
        )

        # Fetch and add global groups
        global_groups = user_permissions.get_global_permission_groups()
        global_groups_str = ""
        for group in global_groups:
            global_groups_str += "%s `%s`\n" % (group.permission_name, group.permission_uuid)
        if global_groups_str == "":
            global_groups_str = "None"
        em.add_field(name="Global groups", value=global_groups_str, inline=False)

        # Fetch and add guild groups
        if message.guild is not None:
            guild_groups = user_permissions.get_guild_permission_groups(int(message.guild.id))
            guild_groups_str = ""
            for group in guild_groups:
                guild_groups_str += "%s `%s`\n" % (group.permission_name, group.permission_uuid)
            if guild_groups_str == "":
                guild_groups_str = "None"
            em.add_field(name="%s groups" % message.guild.name, value=guild_groups_str, inline=False)

        await message.channel.send(embed=em)

    @classmethod
    def get_command_name(cls):
        return "list-groups"

    @classmethod
    def get_summary_text(cls):
        return "Gets a list of global and server permission groups"

    @classmethod
    def get_help_text(cls):
        return "Gets a list of global and server permission groups\n" \
               "**Usage**: %s" % cls.get_command_name()
