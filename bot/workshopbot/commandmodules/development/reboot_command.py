import os
import sys
from typing import List

import discord

from workshopbot.bot import bot_command, WorkshopBot
from workshopbot.commands import ICommand
from workshopbot.parsing import UserCommand


@bot_command
class RebootCommand(ICommand):
    async def execute(self, bot: WorkshopBot, message: discord.Message, user_cmd: UserCommand):
        await message.channel.send("Restarting the bot...")
        await bot.close()
        os.execl(sys.executable, sys.executable, *sys.argv)
        exit(0)

    @classmethod
    def get_command_name(cls):
        return "reboot"

    @classmethod
    def get_summary_text(cls):
        return "Restarts the bot"

    @classmethod
    def get_help_text(cls):
        return "Restarts the bot\n" \
               "**Usage**: %s" % cls.get_command_name()

    @classmethod
    def get_global_permission_group_whitelist(cls) -> List[str]:
        return ["developer"]

    @classmethod
    def get_global_permission_group_blacklist(cls) -> List[str]:
        return ["@everyone"]
