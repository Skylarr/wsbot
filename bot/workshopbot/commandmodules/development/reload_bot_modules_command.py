import importlib
import logging
import sys
from typing import List

import discord

import workshopbot
from workshopbot.bot import bot_command, WorkshopBot
from workshopbot.commands import ICommand
from workshopbot.parsing import UserCommand


@bot_command
class ReloadBotModulesCommand(ICommand):
    async def execute(self, bot: WorkshopBot, message: discord.Message, user_cmd: UserCommand):
        prev_command_count = len(workshopbot.bot.registered_commands)
        prev_module_count = len(sys.modules)
        reloaded_count = 0
        workshopbot.bot.registered_commands.clear()

        current_modules = dict(sys.modules)

        for name, module in current_modules.items():
            if not name.startswith("workshopbot."):
                continue

            importlib.reload(module)
            reloaded_count += 1
            logging.info("Reloaded module %s" % name)

        new_command_count = len(workshopbot.bot.registered_commands)
        new_module_count = len(sys.modules)
        await message.channel.send(
            "%d modules reloaded (%d -> %d total modules)\nUnregistered %d commands, registered %d commands" %
            (reloaded_count, prev_module_count, new_module_count, prev_command_count, new_command_count)
        )

    @classmethod
    def get_command_name(cls):
        return "reload-bot-modules"

    @classmethod
    def get_summary_text(cls):
        return "Unloads and reloads all bot modules"

    @classmethod
    def get_help_text(cls):
        return "Unloads and reloads all bot modules\n" \
               "**Usage**: %s" % cls.get_command_name()

    @classmethod
    def get_global_permission_group_whitelist(cls) -> List[str]:
        return ["developer"]

    @classmethod
    def get_global_permission_group_blacklist(cls) -> List[str]:
        return ["@everyone"]
