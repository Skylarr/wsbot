import json
from typing import List, Dict

import discord

from workshopbot.smb import smb_db
from workshopbot import database
from workshopbot.bot import bot_command, WorkshopBot
from workshopbot.commands import ICommand
from workshopbot.parsing import UserCommand


@bot_command
class TestCommand(ICommand):
    async def execute(self, bot: WorkshopBot, message: discord.Message, user_cmd: UserCommand):
        # await bot.send_message(message.channel, "Is developer? %s" %
        #                        str(user_permissions.is_user_in_global_group(message.author.id, "developer")))

        f = open("stages.json", "r")
        stages_str = f.read()
        f.close()

        stages: Dict = json.loads(stages_str)

        for root_k, root_v in stages.items():
            if root_k == "packs":
                for pack in root_v:
                    await message.channel.send("Found pack %s by %s with %d stages" %
                                               (pack["name"], pack["creator"], len(pack["stages"])))

                    creator_uuid = smb_db.add_creator(pack["creator"])

                    for stage in pack["stages"]:
                        stage_name = stage.get("stage_name")
                        stage_description = stage.get("stage_description")
                        stage_image_url = None
                        smb_db.add_stage_batched(stage_name, creator_uuid, stage_description, stage_image_url)
                        print("%d" % stage.get("stage_slot"))

                    database.commit()
                    await message.channel.send("Added %d stages to the db" % (len(pack["stages"]),))

    @classmethod
    def get_command_name(cls):
        return "test"

    @classmethod
    def get_summary_text(cls):
        return "A test command, for testing purposes"

    @classmethod
    def get_help_text(cls):
        return "A test command, for testing purposes\n" \
               "**Usage**: %s" % cls.get_command_name()

    @classmethod
    def get_global_permission_group_whitelist(cls) -> List[str]:
        return ["developer"]

    @classmethod
    def get_global_permission_group_blacklist(cls) -> List[str]:
        return ["@everyone"]
