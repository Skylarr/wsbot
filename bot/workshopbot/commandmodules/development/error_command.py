from typing import List

import discord

from workshopbot.bot import bot_command, WorkshopBot
from workshopbot.commands import ICommand
from workshopbot.parsing import UserCommand


@bot_command
class ErrorCommand(ICommand):
    async def execute(self, bot: WorkshopBot, message: discord.Message, user_cmd: UserCommand):
        raise Exception("Error requested")

    @classmethod
    def get_command_name(cls):
        return "err"

    @classmethod
    def get_summary_text(cls):
        return "Throws an error"

    @classmethod
    def get_help_text(cls):
        return "Throws an error\n" \
               "**Usage**: %s" % cls.get_command_name()

    @classmethod
    def get_global_permission_group_whitelist(cls) -> List[str]:
        return ["developer"]

    @classmethod
    def get_global_permission_group_blacklist(cls) -> List[str]:
        return ["@everyone"]
