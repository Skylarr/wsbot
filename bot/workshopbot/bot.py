import errno
import logging
import sys
import time
import traceback

import discord

from workshopbot import (database, databasemagic, parsing, private_config,
                         user_permissions)

registered_commands = {}


def bot_command(func):
    registered_commands[func.get_command_name()] = func
    logging.info("Registered command %s" % func.get_command_name())

    def wrapper():
        func()

    return wrapper


class WorkshopBot(discord.Client):
    def __init__(self):
        super().__init__()

        self.command_prefix = "]"
        self.init_time = time.time()

    def run(self):
        try:
            logging.info("Logging in to Discord...")
            super().run(private_config.DISCORD_TOKEN)  # wsbot
        except discord.LoginFailure:
            logging.error("Failed to login!")
            traceback.print_exc()
            exit(errno.EPERM)

    async def on_ready(self):
        logging.info("Logged in as %s#%s [%s]" % (self.user.name, self.user.discriminator, self.user.id))
        await self.change_presence(activity=discord.Game(name=self.command_prefix + "help"))

    async def on_message(self, message: discord.Message):
        try:
            if message.author.bot:
                return
            if not message.content.startswith(self.command_prefix):
                return
            if message.content.startswith(self.command_prefix + self.command_prefix):  # Double prefix = reply
                return

            cmdline_str = message.content[len(self.command_prefix):]
            cmdline = parse_cmdline(cmdline_str)
            # await self.send_message(message.channel, "**Tree**\n```\n%s```" % cmdline)

            # Prevent bot spam
            if len(cmdline.commands) > 5:
                em = discord.Embed(
                    title="⚠️ You can only chain a maximum of 5 commands",
                    color=0xF44336
                )
                await message.channel.send(embed=em)
                return

            # Execute each command now
            for user_cmd in cmdline.commands:
                try:
                    if user_cmd.argv[0] in registered_commands:
                        # Check if the user is permitted
                        # TODO: Batch this all into one query
                        cmd_cls = registered_commands[user_cmd.argv[0]]

                        has_permissions = "@everyone" not in cmd_cls.get_global_permission_group_blacklist()
                        for group in cmd_cls.get_global_permission_group_whitelist():
                            if user_permissions.is_user_in_global_group(message.author.id, group):
                                has_permissions = True
                                break

                        if has_permissions:
                            logging.info("Received command %s" % " ".join(user_cmd.argv))
                            cmd = cmd_cls()

                            await cmd.execute(self, message, user_cmd)
                        else:
                            # No permissions
                            em = discord.Embed(
                                title="🔐 You don't have permission to run %s" % user_cmd.argv[0],
                                color=0xF44336
                            )
                            await message.channel.send(embed=em)
                    else:
                        em = discord.Embed(
                            title="⚠️ Not a command: %s" % user_cmd.argv[0],
                            description="Use %shelp for a list of commands" % self.command_prefix,
                            color=0xF44336
                        )
                        await message.channel.send(embed=em)

                except:
                    # Some error occurred while executing the command
                    traceback.print_exc()

                    exc_string = "".join(traceback.format_exception(*sys.exc_info()))
                    if len(exc_string) > 1013:
                        exc_string = exc_string[:1013] + "..."

                    await message.channel.send(
                        "⚠ **An error occurred whilst executing the command**\n```%s```" % exc_string
                    )

        except:
            # Some error occurred while parsing the command
            exc_string = "".join(traceback.format_exception(*sys.exc_info()))
            if len(exc_string) > 1013:
                exc_string = exc_string[:1013] + "..."

            await message.channel.send(
                "⚠ **An error occurred whilst parsing the command**\n```%s```" % exc_string
            )

            raise


def parse_cmdline(cmdline: str):
    raw_tree = parsing.parse(cmdline)
    t = parsing.Transform()
    t.visit(raw_tree)
    return t.cmd_line
