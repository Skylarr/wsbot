from workshopbot.smb.smb_creator import SmbCreator


class SmbStage:
    def __init__(self, name: str = None, description: str = None, creator: SmbCreator = None):
        self.name = name
        self.description = description
        self.creator = creator
